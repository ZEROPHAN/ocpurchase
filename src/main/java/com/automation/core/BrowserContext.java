package com.automation.core;

import com.automation.base.TestContextManager;
import com.automation.base.TestEnvironments;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;


public class BrowserContext {
	protected WebDriver driver;
	private static int BROWSER_WIDTH = 1280;
	private static int BROWSER_HEIGHT = 720;

	public BrowserContext() {
//		DesiredCapabilities capabilities = DesiredCapabilities.firefox();
//		capabilities.setCapability("marionette", true);
//		capabilities.setCapability("browserName", "chrome");
//
//		// Set driver path
//		String os = System.getProperty("os.name");
//		String platform = System.getProperty("os.arch");
//
//		if (os.toUpperCase().contains("WINDOWS")) {
//			capabilities.setCapability("platform", "WINDOWS");
//			if (platform.contains("64")) {
//				System.setProperty("webdriver.gecko.driver", "lib/geckodriver windows 0.17.0 x64.exe");
//				try {
//					capabilities.setCapability("chrome_binary", DataLoader.readPropertyValue("/Environment.properties", "path.firefox.windows64"));
//				} catch (IOException e) {
//					System.out.println("Binary not found, getting default binary path for Windows");
//				}
//			} else {
//				System.setProperty("webdriver.gecko.driver", "lib/geckodriver windows 0.17.0 x86.exe");
//				try {
//					capabilities.setCapability("Chrome_binary", DataLoader.readPropertyValue("/Environment.properties", "path.chrome.windows"));
//				} catch (IOException e) {
//					System.out.println("Binary not found, getting default binary path for Windows");
//				}
//			}
//		}
//		if (os.toUpperCase().contains("LINUX")) {
//			capabilities.setCapability("platform", "LINUX");
//			try {
//				capabilities.setCapability("chrome_binary", DataLoader.readPropertyValue("/Environment.properties", "path.chrome.linux"));
//			} catch (IOException e) {
//				System.out.println("Binary not found, getting default environment for Linux");
//			}
//			if (platform.contains("64"))
//				System.setProperty("webdriver.gecko.driver", "lib/geckodriver_linux_0.17.0_x64");
//			else
//				System.setProperty("webdriver.gecko.driver", "lib/geckodriver_linux_0.17.0_x86");
//		}
		ChromeOptions chromeOptions = new ChromeOptions();
		initChrome(chromeOptions);

		// Initilize driver
		driver = new ChromeDriver(chromeOptions);

		// Set screen size
		if (TestContextManager.getTestContext().getEnvironment() != TestEnvironments.PRODUCTION) {
			driver.manage().window().setPosition(new Point(0, 0));
			driver.manage().window().setSize(new Dimension(BROWSER_WIDTH, BROWSER_HEIGHT));
		}
	}
	static private void initChrome(ChromeOptions chromeOptions) {
		WebDriverManager.chromedriver().clearDriverCache();
		WebDriverManager.chromedriver().setup();
		chromeOptions.addArguments("--allow-running-insecure-content");
		chromeOptions.addArguments("--ignore-certificate-errors");
		chromeOptions.addArguments("disable-infobars");
		chromeOptions.addArguments("--disable-web-security");
	}
	public WebDriver getDriver() {return driver;}
	public void setDriver(WebDriver driver) {
		this.driver = driver;
	}

	public void quitApp() {
		if (driver != null) {
			driver.quit();
			driver = null;
		}
	}
}
