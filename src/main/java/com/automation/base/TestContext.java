package com.automation.base;

import io.cucumber.java.Scenario;

public class TestContext {
	private Scenario scenario;
    private TestEnvironments environment;

    public TestContext(Scenario scenario) throws Exception {
    	setEnvironment(TestContextManager.getTestEnvironmentFromConfig());
    	setScenario(scenario);
    }
	public TestEnvironments getEnvironment() {
		return environment;
	}

	public void setEnvironment(TestEnvironments environment) {
		this.environment = environment;
	}

	public Scenario getScenario() {
		return scenario;
	}

	public void setScenario(Scenario scenario) {
		this.scenario = scenario;
	}
    
    
}
