package com.automation.base;


import com.automation.core.WebApp;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import static org.apache.logging.log4j.LogManager.getLogger;

abstract public class WebPageBase {
	protected WebDriver driver;
	protected WebApp app;
	protected Logger log = getLogger(this.getClass());

	abstract protected void init(Object[] params) throws Exception;

	abstract protected void checkOnPage() throws Exception;

	// Constructor
	public WebPageBase() throws Exception {
	}

	public void initPageObject(Object[] params) throws Exception {
		// call init in derived class
		init(params);
		// call checkOnPage in derived class
		checkOnPage();
	}

}
