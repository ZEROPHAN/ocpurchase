package com.automation.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

public class DataLoader {
	/**
	 * Reads the value from a given properties file
	 * 
	 * @param file
	 *            is the path to properties file to read from
	 * @param key
	 *            is the key to read the value for
	 * @throws IOException 
	 */
	public static String readPropertyValue(String file, String key) throws IOException {
		Properties prop = new Properties();
		InputStream input = null;
		String result = null;
		try {
			input = DataLoader.class.getResourceAsStream(file);
			prop.load(input);
			result = prop.getProperty(key);
		} finally {
			if (input != null) {
				input.close();
			}
		}
		return result;
	}
	
	public static List<String> readPropertyValues(String file, String key) throws IOException {
		Properties prop = new Properties();
		InputStream input = null;
		List<String> results = null;
		List<String> trimmedStrings;
		try {
			input = DataLoader.class.getResourceAsStream(file);
			prop.load(input);
			String[] values = prop.getProperty(key).split(",");
			results = new ArrayList<String>();// import ArrayList và results.
			for (String value : values) {
				results.add(value.trim());
			}
			trimmedStrings = Arrays.asList(values).stream().map(String::trim).collect(Collectors.toList());

		} finally {
			if (input != null) {
				input.close();
			}
		}
		return trimmedStrings;
	}

}
