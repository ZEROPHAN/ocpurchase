package com.context;

import java.util.List;

import com.unity.Order;
import com.unity.Purpose;

public class PurchaseContext {
	private Order order;
	private Purpose purpose;
	private List<Order> orders;
	
	/** 
	 * Generates all contexts to use in a scenario
	 */
	public PurchaseContext() {
		order = new Order();
	}
		
	public Order getOrder() {
		return order;
	}
	
	public void setOrder(Order order) {
		this.order = order;
	}

	public Purpose getPurpose() {
		return purpose;
	}

	public void setPurpose(Purpose purpose) {
		this.purpose = purpose;
	}

	public List<Order> getOrders() {
		return orders;
	}

	public void setOrders(List<Order> orders) {
		this.orders = orders;
	}
}
