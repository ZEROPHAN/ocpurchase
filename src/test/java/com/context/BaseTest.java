package com.context;

import com.automation.base.TestContextManager;
import com.automation.base.TestEnvironments;
import com.automation.core.WebAppManager;
import com.pageobjects.common.PurchaseWebApp;
import com.unity.Order;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import io.cucumber.java.Status;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 * Class to hold Before- and AfterScenario actions
 */
public class BaseTest {
	Logger log = LogManager.getLogger(this.getClass());
	
	public BaseTest() throws Throwable {
	}

	@Before
	public void beforeScenario(Scenario scenario) throws Throwable {
		log.info("========================");
		log.info("Starting scenario: " + scenario.getName());
		log.info("========================");
		TestContextManager.createContext(scenario);
		if (TestContextManager.getTestContext().getEnvironment() == null) {
			log.info("No environment set, DEVELOPMENT is selected by default");
			TestContextManager.getTestContext().setEnvironment(TestEnvironments.DEVELOPMENT);
		}
		PurchaseWebApp app = (PurchaseWebApp) WebAppManager.getWebApp(PurchaseWebApp.class);
		app.getBrowserContext().getDriver().manage().deleteAllCookies();
	}

	@After
	public void afterScenario(Scenario scenario) throws Throwable {
		PurchaseWebApp app = (PurchaseWebApp) WebAppManager.getWebApp(PurchaseWebApp.class);
		Order order = app.getContext().getOrder();
		try {
			Status testStatus = scenario.getStatus();
			if (testStatus == Status.PASSED) {
				log.info("Test scenario passed.");
			} else if (testStatus == Status.SKIPPED) {
				log.info("Test scenario [" + scenario.getName() + "] skipped.");
			} else if (testStatus == Status.FAILED) {
				log.info("Test scenario [" + scenario.getName() + "] failed.");
				WebAppManager.takeAppScreenshots(scenario.getName());
			} else {
				log.error("Invalid test status!");
			}			
		} catch (Exception e) {
			log.error(e.toString());
			e.printStackTrace();
		} finally {
			WebAppManager.quitApp();
			log.info("========================");
			log.info("Scenario ended: " + scenario.getName());
			log.info("========================");
		}
	}
}