package com.cucumber;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;


@RunWith(Cucumber.class)
@CucumberOptions(
        glue = {"com.context", "com.steps", "com.automation.base"}
        , features = {"src/test/scenarios/"}
        , plugin = {"pretty", "html:target/cucumber", "json:target/cucumber.json", "junit:target/junit.xml"})
public class CucumberRunner {

}
