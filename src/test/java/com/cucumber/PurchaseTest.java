package com.cucumber;

import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
        tags = "@purchase",
        plugin = {"pretty", "json:target/cucumber-report/Cucumber.json", "html:target/cucumber"},
        features = {"src/test/resources/cucumber/"})
class PurchaseTest extends CucumberRunner {

}
