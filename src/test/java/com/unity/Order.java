package com.unity;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;

public class Order {
    private int id;
    private String productLink;
    private DateTime submitTime;
    private DateTime loadedTime;
    private long timeoutMillis;
    private double purchaseValue;
    private List<Product> products = new ArrayList<Product>();
    private int isFailure;
    private int isValidated;

    public Order() {
    }

    /**
     * @return the productLink
     */
    public String getProductLink() {
        return productLink;
    }

    /**
     * @param productLink the productLink to set
     */
    public void setProductLink(String productLink) {
        this.productLink = productLink;
    }

    /**
     * @return the dateTime
     */
    public DateTime getSubmitTime() {
        return submitTime;
    }

    /**
     * @param dateTime the dateTime to set
     */
    public void setDateTime(DateTime dateTime) {
        this.submitTime = dateTime;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the purchaseValue
     */
    public double getPurchaseValue() {
        return purchaseValue;
    }

    /**
     * @param purchaseValue the purchaseValue to set
     */
    public void setPurchaseValue(double purchaseValue) {
        this.purchaseValue = purchaseValue;
    }

    /**
     * @return the products
     */
    public List<Product> getProducts() {
        return products;
    }

    /**
     * @param products the products to set
     */
    public void setProducts(List<Product> products) {
        this.products = products;
    }

    /**
     * Return the time the order appear on Publisher page
     */
    public DateTime getLoadedTime() {
        return loadedTime;
    }

    /**
     * @param loadedTime time when the order appear on Publisher page
     */
    public void setLoadedTime(DateTime loadedTime) {
        this.loadedTime = loadedTime;
    }

    public int getIsFailure() {
        return isFailure;
    }

    public void setIsFailure(int isFailure) {
        this.isFailure = isFailure;
    }

    public int getIsValidated() {
        return isValidated;
    }

    public void setIsValidated(int isValidated) {
        this.isValidated = isValidated;
    }

    public long getTimeoutMillis() {
        return timeoutMillis;
    }

    public void setTimeoutMillis(long timeoutMillis) {
        this.timeoutMillis = timeoutMillis;
    }
}
