package com.pageobjects.common;

import com.automation.core.WebApp;
import com.context.PurchaseContext;

public class PurchaseWebApp extends WebApp {
	private PurchaseContext context;
	private String currentURL;

	public PurchaseWebApp() {
		super();
		this.context = new PurchaseContext();
	}

	public PurchaseContext getContext() {
		return this.context;
	}

	public void setContext(PurchaseContext context) {
		this.context = context;
	}

	public String getCurrentURL() {
		return currentURL;
	}

	public void setCurrentURL(String currentURL) {
		this.currentURL = currentURL;
	}
}
