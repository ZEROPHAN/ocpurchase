package com.pageobjects.common;


import com.automation.core.WebAppManager;
import com.unity.Order;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class PurchaseSteps {
	protected PurchaseWebApp app;
	protected Logger log;
	protected Order order;
	
	public PurchaseSteps() throws Throwable {
		app = (PurchaseWebApp) WebAppManager.getWebApp(PurchaseWebApp.class);
		log = LogManager.getLogger(this.getClass());
		order = app.getContext().getOrder();
	}

	protected boolean pageIsTheSame() {
		String getUrl = app.getBrowserContext().getDriver().getCurrentUrl();
		if (getUrl.equals(app.getCurrentURL())) {
			return true;
		} else {
			app.setCurrentURL(getUrl);
			return false;
		}
	}
}
